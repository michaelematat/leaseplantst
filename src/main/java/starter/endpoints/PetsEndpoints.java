package starter.endpoints;

public class PetsEndpoints {

    public static final String PET = "/pet";
    public static final String PET_BY_ID = "/pet/{petId}";
    public static final String PET_BY_STATUS = "/pet/findByStatus";
}
