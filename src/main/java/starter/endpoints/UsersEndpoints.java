package starter.endpoints;

public class UsersEndpoints {

    public static final String USER = "/user";
    public static final String USER_LOGIN = "/user/login";
    public static final String USER_BY_USERNAME = "/user/{username}";
}

