package starter.endpoints;

public class StoreEndpoints {

    public static final String ORDER = "/store/order";
    public static final String ORDER_BY_ID = "/store/order/{orderId}";
    public static final String STORE_INVENTORY = "/store/inventory";
}
