@UserDeletion
Feature: User deletion

  Scenario: User deletion after its creation
    Given I create a new user
    Then I delete newly created user
    And check that user is deleted