@UserUpdate
Feature: User info update
  Background:
    Given I create a new user

  @ValidUserUpdate
  Scenario: Updating info of existing user
    Given I update newly created user with new random last name
    And check that user has been updated

  @InvalidUserUpdate
  Scenario: Update user with invalid data
    Given I update newly created user with invalid data
    Then check that user update failed with error message