@UserInfo
Feature: Get user info
  Background:
    Given I create a new user

  @GetUser
  Scenario: Getting info of the existing user
    Given I get info of the user
    And check the user info

  @GetInvalidUser
  Scenario: Getting info of non existent user
    Given I get info of the non existent user
    Then check error message and verify that user does not exist