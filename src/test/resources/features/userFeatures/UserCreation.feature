@UserCreation @Smoke
Feature: User creation

  @ValidUserCreation
  Scenario: Creating new user with valid data
    Given I create a new user
    Then I should get the response that user is created

  @InvalidUserCreation
  Scenario: Creating new user with invalid data
    Given I try to create new user with invalid data
    Then check that user creation is unsuccessful