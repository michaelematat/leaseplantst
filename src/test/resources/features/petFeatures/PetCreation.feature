@PetCreation @Smoke
Feature: Pet creation

  Scenario: Pet creation with valid data
    Given I create new pet
    And check that pet was created


  Scenario: Pet creation with invalid data
    Given I create new pet with invalid data
    And check that pet creation failed
