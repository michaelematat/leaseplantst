@PetSearching
Feature: Pet searching
  Background:
    Given I create new pet

  Scenario: Find a new pet by id
    Given I search for new pet by id
    Then check that pet was found

  Scenario:  Find a new pet by status
    Given I search for new pet by available status
    Then check that pet was found in the list

  Scenario: Look for non existent pet
    Given I search for new pet by non existent id
    Then check that pet was not found