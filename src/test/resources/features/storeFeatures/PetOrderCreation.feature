@PetOrderCreation
Feature: Pet creation
  Background:
    Given I create new pet in store

  @OrderCreation
  Scenario: Login as existing user and placing order for the pet
    Given I log in as existing user
    And check that login was successful
    Then I place order for a pet
    And check that order has been created successfully

  @InvalidOrderCreation
  Scenario: Login as existing user and trying to place an invalid order
    Given I log in as existing user
    And check that login was successful
    Then I try to create invalid pet order
    And check that order placement has failed