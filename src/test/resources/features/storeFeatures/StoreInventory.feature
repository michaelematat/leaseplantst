@StoreInventory
Feature: Store inventory

  Scenario: Creating new pet and verifying that inventory was updated
    Given I check available pets in the inventory
    Then I create new pet in store
    And I verify that new pet was added to the inventory
