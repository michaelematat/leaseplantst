package starter.actions;
import com.github.javafaker.Faker;
import net.serenitybdd.rest.SerenityRest;
import org.json.JSONObject;
import starter.models.Category;
import starter.models.Pet;
import starter.models.Tag;
import java.util.Arrays;
import java.util.List;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static net.serenitybdd.rest.SerenityRest.then;
import static org.hamcrest.Matchers.equalTo;
import static starter.Constants.BASE_URI;
import static starter.endpoints.PetsEndpoints.*;

public class PetActions {
    private int id = Faker.instance().random().nextInt(10000);
    private String petName = Faker.instance().name().name();
    private Category category = new Category(Faker.instance().random().nextInt(10),
                                          Faker.instance().funnyName().name());
    private String status = "available";
    private List<String> photoUrls = Arrays.asList(Faker.instance().internet().url());
    private List<Tag> tags = Arrays.asList(new Tag(Faker.instance().random().nextInt(50),
                                        Faker.instance().name().name()));
    private Pet pet = new Pet(id, category, petName,  photoUrls,  tags,status);
    private JSONObject petData = new JSONObject(pet);

    public int createPetAndGetId(){
        SerenityRest.given()
                .baseUri(BASE_URI)
                .contentType("application/json")
                .body(petData.toString())
                .when()
                .post(PET);
        return then().extract().path("id");
    }

    public void createPetWithInvalidData(String petData){
        SerenityRest.given()
                .baseUri(BASE_URI)
                .contentType("application/json")
                .body(petData)
                .when()
                .post(PET);
    }

    public void verifyPetData(){
        restAssuredThat(response -> response
                .statusCode(200)
                .body("",equalTo(petData.toMap())));
    }

    public void findPetById(int id){
        SerenityRest.given()
                .baseUri(BASE_URI)
                .pathParam("petId", id)
                .contentType("application/json")
                .when()
                .get(PET_BY_ID);
    }

    public void findPetByStatus(String status){
        SerenityRest.given()
                .baseUri(BASE_URI)
                .queryParam("status", status)
                .contentType("application/json")
                .when()
                .get(PET_BY_STATUS);
    }
}
