package starter.actions;
import net.serenitybdd.rest.SerenityRest;
import org.json.JSONObject;
import starter.models.User;
import static starter.endpoints.UsersEndpoints.*;
import static starter.Constants.*;

public class UserActions {

    public void createNewUser (User userData){
        SerenityRest.given()
                .baseUri(BASE_URI)
                .contentType("application/json")
                .body(new JSONObject(userData).toString())
                .when()
                .post(USER);
    }

    public void createNewUser(String userData){    //This method is overloaded since it is the only way to get the 400 status code.
                                                   // In the real case, it wouldn't occur.
        SerenityRest.given()
                .baseUri(BASE_URI)
                .contentType("application/json")
                .body(userData)
                .when()
                .post(USER);
    }

    public void userLogin(String username, String password){
        SerenityRest.given()
                .baseUri(BASE_URI)
                .auth().preemptive()
                .basic(username, password)
                .when()
                .get(USER_LOGIN);
    }

    public void getUserInfo(String username){
        SerenityRest.given()
                .pathParam("username", username)
                .baseUri(BASE_URI)
                .when()
                .get(USER_BY_USERNAME);
    }

    public void updateUser(JSONObject userData, String username){
        SerenityRest.given()
                .pathParam("username", username)
                .baseUri(BASE_URI)
                .contentType("application/json")
                .body(userData.toString())
                .when()
                .put(USER_BY_USERNAME);
    }

    public void updateUser(String userData, String username){ //This method is overloaded since it is the only way to get the 400 status code.
                                                              // In the real case, it wouldn't occur.
        SerenityRest.given()
                .pathParam("username", username)
                .baseUri(BASE_URI)
                .contentType("application/json")
                .body(userData)
                .when()
                .put(USER_BY_USERNAME);
    }

    public void deteleUser(String username){
        SerenityRest.given()
                .pathParam("username", username)
                .baseUri(BASE_URI)
                .when()
                .delete(USER_BY_USERNAME);
    }
}
