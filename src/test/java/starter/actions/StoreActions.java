package starter.actions;

import com.github.javafaker.Faker;
import net.serenitybdd.rest.SerenityRest;
import org.json.JSONObject;
import starter.models.Order;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static net.serenitybdd.rest.SerenityRest.then;
import static org.hamcrest.Matchers.equalTo;
import static starter.Constants.BASE_URI;
import static starter.endpoints.StoreEndpoints.*;

public class StoreActions {
    private int id = Faker.instance().random().nextInt(100);
    private int quantity = Faker.instance().random().nextInt(10);
    private String status = "placed";
    private JSONObject orderData;

    public int createOrder(int petId) {
        Order order = new Order(id, petId, quantity, status, true);
        orderData = new JSONObject(order);
        SerenityRest.given()
                .baseUri(BASE_URI)
                .contentType("application/json")
                .body(orderData.toString())
                .when()
                .post(ORDER);
        return then().extract().path("id");
    }

    public void createOrder(String orderData) {  //This method is overloaded since it is the only way to get the 400 status code.
                                                 // In the real case, it wouldn't occur.
        SerenityRest.given()
                .baseUri(BASE_URI)
                .contentType("application/json")
                .body(orderData)
                .when()
                .post(ORDER);
    }

    public void verifyOrderData() {
        restAssuredThat(response -> response
                .statusCode(200)
                .body("", equalTo(orderData.toMap())));
    }

    public int getAvailablePetsAmount() {
        SerenityRest.given()
                .baseUri(BASE_URI)
                .contentType("application/json")
                .when()
                .get(STORE_INVENTORY);
        return then().extract().path("available");
    }
}
