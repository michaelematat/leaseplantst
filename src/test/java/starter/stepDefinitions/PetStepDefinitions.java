package starter.stepDefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import starter.actions.PetActions;

import static org.hamcrest.Matchers.*;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.equalTo;


public class PetStepDefinitions {
    PetActions petActions = new PetActions();
    private int petId;

    /* Pet creation steps */

    @Given("I create new pet")
    public void createNewPet() {
        petId = petActions.createPetAndGetId();
    }

    @And("check that pet was created")
    public void checkThatPetWasCreated() {
        petActions.verifyPetData();
    }

    @Given("I create new pet with invalid data")
    public void iCreateNewPetWithInvalidData() {
        petActions.createPetWithInvalidData("dumbdata");
    }

    @And("check that pet creation failed")
    public void checkThatPetCreationFailed() {
        restAssuredThat(response -> response
                .statusCode(400)
                .body("message", equalTo("bad input")));
    }

    /* Pet searching steps */

    @Given("I search for new pet by id")
    public void searchForNewPetById() {
        petActions.findPetById(petId);
    }

    @Given("^I search for new pet by (.*) status$")
    public void searchForNewPetByStatus(String status) {
        petActions.findPetByStatus(status);
    }

    @Then("check that pet was found")
    public void checkTheFoundPet() {
        restAssuredThat(response -> response
                .statusCode(200)
                .body("id", equalTo(petId)));
    }

    @Then("check that pet was found in the list")
    public void checkThatPetWasFoundInTheList() {
        restAssuredThat(response -> response
                .statusCode(200)
                .body("id", hasItem(petId)));
    }

    @Given("I search for new pet by non existent id")
    public void searchForNewPetByNonExistentId() {
        petActions.findPetById(909090909);
    }

    @Then("check that pet was not found")
    public void checkThatPetWasNotFound() {
        restAssuredThat(response -> response
                .statusCode(404)
                .body("message", equalTo("Pet not found"))
                .body("type", equalTo("error")));
    }
}
