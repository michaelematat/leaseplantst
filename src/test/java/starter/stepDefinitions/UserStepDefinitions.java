package starter.stepDefinitions;

import com.github.javafaker.Faker;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

import org.json.JSONObject;
import starter.models.User;
import starter.actions.UserActions;

import java.util.HashMap;
import java.util.Map;

import static starter.Constants.*;
import static org.hamcrest.Matchers.*;


public class UserStepDefinitions {
    UserActions userActions = new UserActions();
    private int id = Faker.instance().random().nextInt(10000);
    private String username = Faker.instance().name().username();
    private String firstname = Faker.instance().name().firstName();
    private String email = Faker.instance().internet().emailAddress();
    private String phone = Faker.instance().phoneNumber().phoneNumber();
    private String lastname = Faker.instance().name().lastName();
    private String password = Faker.instance().internet().password();
    private int userStatus = Faker.instance().random().nextInt(10);
    private User userData = new User(id, username, firstname,
            lastname, email, password,
            phone, userStatus);

    /* User creation steps*/

    @Given("I create a new user")
    public void createNewUser() {
        userActions.createNewUser(userData);
    }

    @Given("I try to create new user with invalid data")
    public void tryToCreateNewUserWithInvalidData() {
        userActions.createNewUser("dumbdata");
    }

    @Then("I should get the response that user is created")
    public void getTheResponseStatusCode() {
        restAssuredThat(response -> response
                .statusCode(200)
                .body("message", is(notNullValue(String.class))));
    }

    @Then("check that user creation is unsuccessful")
    public void checkThatUserCreationUnsuccessful() {
        restAssuredThat(response -> response
                .statusCode(400)
                .body("message", equalTo("bad input")));
    }

    /* User deletion steps */

    @Then("I delete newly created user")
    public void deleteNewlyCreatedUser() {
        userActions.deteleUser(username);
    }

    @And("check that user is deleted")
    public void checkThatUserIsDeleted() {
        restAssuredThat(response -> response
                .statusCode(200)
                .body("message", equalTo(username)));
    }

    /* User update steps */

    @Given("I update newly created user with new random last name")
    public void updateExistingUser() {
        Map<String, String> map = new HashMap<>();
        map.put("lastName", lastname);
        JSONObject userData = new org.json.JSONObject(map);
        userActions.updateUser(userData, username);
    }

    @Given("I update newly created user with invalid data")
    public void updateExistingUserWithInvalidData() {
        userActions.updateUser("dumbdata", username);
    }

    @And("check that user has been updated")
    public void checkThatUserHasBeenUpdated() {
        restAssuredThat(response -> response
                .statusCode(200)
                .body("message", is(notNullValue(String.class))));
        userActions.getUserInfo(username);
        restAssuredThat(response -> response
                .body("lastName", equalTo(lastname)));
    }

    @Then("check that user update failed with error message")
    public void checkThatUserUpdateFailedWithErrorMessage() {
        restAssuredThat(response -> response
                .statusCode(400)
                .body("message", equalTo("bad input")));
    }

    /* User info steps */

    @Given("I get info of the user")
    public void getInfoOfTheUser() {
        userActions.getUserInfo(username);
    }

    @Given("I get info of the non existent user")
    public void getInfoOfNonExistentUser() {
        userActions.getUserInfo("dumbdataOfNonExistentUser");
    }

    @And("check the user info")
    public void checkTheUserInfo() {
        restAssuredThat(response -> response
                .statusCode(200)
                .body("id", is(notNullValue(Integer.class))));
    }

    @Then("check error message and verify that user does not exist")
    public void checkErrorMessageThatUserDoesNotExist() {
        restAssuredThat(response -> response
                .statusCode(404)
                .body("message", equalTo("User not found"))
                .body("type", equalTo("error")));
    }

    /* User login steps */

    @Given("I log in as existing user")
    public void logInAsExistingUser() {
        userActions.userLogin(USERNAME, USER_PASSWORD);
    }

    @Then("check that login was successful")
    public void checkThatLoginWasSuccessful() {
        restAssuredThat(response -> response
                .statusCode(200)
                .body("message", containsStringIgnoringCase("logged in user session:")));
    }
}
