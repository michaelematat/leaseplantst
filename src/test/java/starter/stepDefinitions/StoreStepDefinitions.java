package starter.stepDefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import starter.actions.PetActions;
import starter.actions.StoreActions;

import static org.hamcrest.Matchers.*;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;


public class StoreStepDefinitions {
    PetActions petActions = new PetActions();
    StoreActions storeActions = new StoreActions();
    int petID;
    int availablePets;

    @Given("I create new pet in store")
    public void createNewPet() {
        petID = petActions.createPetAndGetId();
        petActions.verifyPetData();
    }

    /* Order placement steps */

    @Then("I place order for a pet")
    public void placeOrderForAPet() {
        storeActions.createOrder(petID);
    }

    @And("check that order has been created successfully")
    public void checkThatOrderHasBeenCreatedSuccessfully() {
        storeActions.verifyOrderData();
    }

    @Then("I try to create invalid pet order")
    public void tryToCreateInvalidPetOrder() {
        storeActions.createOrder("dumbdata");
    }

    @And("check that order placement has failed")
    public void checkThatOrderPlacementHasFailed() {
        restAssuredThat(response -> response
                .statusCode(400)
                .body("message", equalTo("bad input")));
    }

    /* Store inventory steps */

    @Given("I check available pets in the inventory")
    public void checkAvailablePetsInTheInventory() {
        availablePets = storeActions.getAvailablePetsAmount();
    }

    @And("I verify that new pet was added to the inventory")
    public void verifyThatNewPetWasAddedToTheInventory() {
        storeActions.getAvailablePetsAmount();
        restAssuredThat(response -> response
                .body("available", equalTo(availablePets + 1)));
    }
}
