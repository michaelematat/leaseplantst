## Installation

Clone repository:

https: `https://gitlab.com/michaelematat/leaseplantst.git`

ssh:  `https://gitlab.com/michaelematat/leaseplantst.git`


 
First of all make sure you have JDK 1.8 installed.

##### Check the current Java version: 

For Linux, Windows, Mac paste in terminal:  

    java -version

##### If java missing on your system you need to install it:

Linux: 
      
    sudo apt-get install openjdk-8-jdk" 

To update java: 
   
    sudo update-alternatives --set java /usr/lib/jvm/jdk1.8.0_version/bin/java"

Windows and Mac: [download](https://java.com/en/download/manual.jsp) and install from the official website 

#### Now we need to install maven

Linux

    "sudo apt install maven"

Check that maven successfully installed:  

    mvn -version

For Windows and Mac: Download .zip, .tar files respectively from:

    https://maven.apache.org/download.cgi 

Then you need to setup Environment path variables, how to do it: 
   
    https://www.baeldung.com/install-maven-on-windows-linux-mac

Great, now we good to go.

# How to run tests

Through terminal:

     mvn clean verify

this will run all tests and generate report `index.html` in `{projectName}\target\site\serenity` folder 

Or simply run `.feature` file of the desired test

Also can run specific categories of test by using tags

    mvn clean verify -Dcucumber.filter.tags="@tagName" 

also you can put multiple tags like this: "@{tag1} and @{tag2}"

# List of tags used in project

`@Smoke @UserLogin
@ValidLogin
@UserCreation
@ValidUserCreation
@VnvalidUserCreation
@UserUpdate
@ValidUserUpdate
@InvalidUserUpdate
@UserInfo
@GetUser
@GetInvalidUser
@UserDeletion
@StoreInventory
@PetOrderCreation
@OrderCreation
@InvalidOrderCreation
@PetSearching
@PetCreation`

# How to write new tests

Create new .feature file in the `test\resources\features`
  * If feature file already exists for your test, add a new scenario inside that feature file.

Create new step definition file (if necessary) inside `test\starter\stepdefinitions`

Add new endpoint to corresponding class inside the `java\starter\endpoints` or create your own if required

New models should be created inside `java\starter\models`

Define actions inside the `java\starter\actions`

Don't forget to update tag list above if you've created a new tag.