# Endpoints
Base uri: https://petstore.swagger.io/
 

| Endpoint        | Method          | Description  | Required data|
| ------------- |:-------------:| :-----------:|  :------------:| 
| `/user`     | POST | create user | Json body|
| `/user/login`      | GET      |    user login | query data {username/password}|
| `/user/{username}` | PUT     |    update user | Json body, username pathParams |
| `/user/{username}` | GET     |    get user info | username path Params |
| `/user/{username}` | DELETE     |    delete user | username path Params |
| `/store/inventory` | GET     |    get store inventory | - |
| `/store/order` | POST     |   place order for pet | Json body |
| `/pet` | POST     |   create pet | Json body |
| `/pet/{petId}` | GET     |   get pet by ID | pet ID path params |
| `/pet/findByStatus` | GET     |    get pet by status | query data {status} |


**Didn't describe any fields for these endpoints since all of them lacking validations and simply taking any values, almost all of these requests could be sent successfully even without required data.**